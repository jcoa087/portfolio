import React from "react";
import './Contact.css'
import { BsFillHouseFill,BsFillTelephoneFill } from "react-icons/bs";
import { MdEmail } from "react-icons/md";

const Contact = ()=>{

    return(
        <section id='contact_element' >
                <a className="contact-card" >Hermosillo Sonora<BsFillHouseFill /></a>
                <a className="contact-card">(+52)662-4806512<BsFillTelephoneFill /></a>
                <a className="contact-card" href="mailto:jcoa087@gmail.com" >jcoa087@gmail.com<MdEmail /></a>

        </section>
    )
}

export default  Contact