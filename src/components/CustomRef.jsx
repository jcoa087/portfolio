import React,{ useRef,createContext,useContext } from "react";

const homeContext = createContext()
const aboutContext = createContext()
const portfolioContext = createContext()
const contactContext = createContext()

export const useHomeContext = ()=>{
    return useContext(homeContext)
}

export const useAboutContext = ()=>{
    return useContext(aboutContext)
}

export const usePortfolioContext = ()=>{
    return useContext(portfolioContext)
}

export const useContactContext = ()=>{
    return useContext(contactContext)
}

export const CustomRef =({children})=>{
    const homeRef=useRef()
    const aboutRef=useRef()
    const portfolioRef=useRef()
    const contactRef=useRef()

    return(
        <homeContext.Provider value={homeRef}>
            <aboutContext.Provider value={aboutRef}>
                <portfolioContext.Provider value={portfolioRef}>
                    <contactContext.Provider value={contactRef}>
                    {children}
                    </contactContext.Provider>
                </portfolioContext.Provider>
            </aboutContext.Provider>
        </homeContext.Provider>
    )
}