import React from 'react'
import "./PortfolioDescription.css"
import { useNavigate } from "react-router-dom";

const PortfolioDescription = () => {
  const navigate = useNavigate()
  
  const projects = ()=>{
    navigate('/projects')
  }
  return (
   <div id="portfolio_description">
        <p>Aqui podrás ver los proyectos en los que trabajo y he trabajado.</p>
        <div className='btn_portfolio' onClick={projects}>Ver proyectos</div>
    </div>
  )
}

export default PortfolioDescription