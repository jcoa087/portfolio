import React from 'react'
import"./Skillset.css"

import Html from "./../../assets/html.svg"
import Laravel from "./../../assets/laravel.svg"
import Javascript from "./../../assets/javascript.svg"
import Php from "./../../assets/php.svg"
import ReactIcon from "./../../assets/react.svg"
import VSCode from "./../../assets/VSCode.svg"
import Git from "./../../assets/git.svg"
import Gitlab from "./../../assets/gitlab.svg"
import Mysql from "./../../assets/mysql.svg"
import Responsive from "./../../assets/responsive.svg"
import Css from "./../../assets/css.svg"
import Bootstrap from "./../../assets/bootstrap.svg"

const Skillset = () => {
  return (
    <div id="skillset">
        
        <div className='skill' data-tooltip='HTML' style={{backgroundImage:`url(${Html})`}}>
          {/* <div className="skill_progress" ></div> */}
        </div>
        {/* <div className='skill' data-tooltip='Laravel' style={{backgroundImage:`url(${Laravel})`}}>
          <div className="skill_progress" ></div>
        </div> */}
        <div className='skill' data-tooltip='Bootstrap' style={{backgroundImage:`url(${Bootstrap})`}}></div>
        <div className='skill' data-tooltip='CSS' style={{backgroundImage:`url(${Css})`}}></div>
        <div className='skill' data-tooltip='Responsive' style={{backgroundImage:`url(${Responsive})`}}></div>
        <div className='skill' data-tooltip='Mysql' style={{backgroundImage:`url(${Mysql})`}}></div>
        <div className='skill' data-tooltip='Visual Studio Code' style={{backgroundImage:`url(${VSCode})`}}></div>
        <div className='skill' data-tooltip='React' style={{backgroundImage:`url(${ReactIcon})`}}></div>
        <div className='skill' data-tooltip='php' style={{backgroundImage:`url(${Php})`}}></div>
        <div className='skill' data-tooltip='Javascript' style={{backgroundImage:`url(${Javascript})`}}></div>
        <div className='skill' data-tooltip='Git' style={{backgroundImage:`url(${Git})`}}></div>
        <div className='skill' data-tooltip='Gitlab' style={{backgroundImage:`url(${Gitlab})`}}></div>

    </div>
  )
}

export default Skillset