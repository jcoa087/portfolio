import React from 'react'
import "./Git.css"
import GitlabIcon from "./../../assets/gitlab.svg"
const Git = () => {
  return (
    <div id="git">

        <a id='git_link' href="https://gitlab.com/jcoa087" target='_blank'>
            <img src={GitlabIcon} alt="" />
        </a>
        
    </div>
  )
}

export default Git