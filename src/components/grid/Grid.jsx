import React from 'react'
import "./Grid.css"

import About from '../about/About'
import HeadAnimation from '../head-animation/HeadAnimation'
import PortfolioDescription from '../portfolio-description/PortfolioDescription'
import Resume from '../resume/Resume'
import Skillset from '../skillset/Skillset'
import Contact from "./../contact/Contact"
import Git from '../git/Git'
import Footer from "./../footer/Footer"
const Grid = () => {
  return (
    <section id='grid'>
      <h1 id='title'>Portafolio</h1>
      <About/>
      <HeadAnimation/>
      <PortfolioDescription/>
      <Git/>
      <Resume/>
      <Skillset/>
      <Contact/>
      <Footer/>
    </section>
  )
}

export default Grid