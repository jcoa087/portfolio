import React, { createContext, useContext, useState } from "react";


const modalContext = createContext()
const info = createContext()

export const useModalContext = ()=>{
    return useContext(modalContext)
}

export const useInfo = ()=>{
    return useContext(info)
}

export const CustomState = ({children})=>{
    const [modal,setModal] =useState(false)
    const[modalInfo,setModalInfo] = useState({})
    return(
        <modalContext.Provider value={[modal,setModal]}>
            <info.Provider value={[modalInfo,setModalInfo]}>
            {children}
            </info.Provider>
            
        </modalContext.Provider>
    )
}