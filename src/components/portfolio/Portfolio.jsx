import React, { useState } from "react";
import './Portfolio.css'
import { usePortfolioContext } from "../CustomRef";
import { useNavigate } from "react-router-dom";
const Portfolio = ()=>{
    const navigate = useNavigate()



    const cards =[
                {name:'To-do list',
                background:"https://images.unsplash.com/photo-1484480974693-6ca0a78fb36b?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8dG8lMjBkbyUyMGxpc3R8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=500&q=60",
                description:'Una app que te permite crear una lista, ademas de eliminar o marcar como terminadas las tareas.',
                link:'https://portfolio-todo-list.xyz/',
                developed:"React, Css"
            },

                {name:'Videojuegos',
                background:"https://images.unsplash.com/photo-1579373903781-fd5c0c30c4cd?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Nnx8dmlkZW9nYW1lc3xlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=500&q=60",
                description:'Pagina web donde puedes ver diferentes videojuegos y su informacion.',
                link:'https://portfolio-gamestore.xyz/',
                developed:"React, Css"
            },

                {name:'Restaurante',
                background:"https://images.unsplash.com/photo-1513104890138-7c749659a591?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8cGl6emF8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=500&q=60",
                description:'Pagina web de comida.',
                link:'https://portfolio-restaurant.xyz/',
                developed:"React, Css"
            },

                {name:'Carrito de compras',
                background:"https://plus.unsplash.com/premium_photo-1681488262364-8aeb1b6aac56?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MzF8fHdlYiUyMGNhcnR8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=500&q=60",
                description:'Carrito de compras con login que permite crear una cuenta y agregar una fotografia.',
                link:'https://portfolio-cart.xyz/',
                developed:"React, css, Php, Composer, JWT"
            },
           ]
       
    const ref = usePortfolioContext()

    const start = ()=>{
        navigate('/')
    }
    return(
        <section   id='portfolio_element'>
            <h2>Proyectos</h2>
            <a className="start-link" onClick={start} >Inicio</a>
            <div  className='portfolio-content'>
                {
                    cards.map((card,idx)=>
                    <div key={idx} className='card' style={{backgroundImage:`url(${card.background})`}}  >
                        <div className='overlay'>
                            <h2>{card.name}</h2>
                            <p>{card.description}<br/> <span>Tecnologias utilizadas:</span> {card.developed}</p>
                            <a href={card.link} target="_blank">Ir a sitio web</a>
                        </div>
                    </div>
                    )
                }
            </div>

            
        </section>
    )
}
 export default Portfolio

