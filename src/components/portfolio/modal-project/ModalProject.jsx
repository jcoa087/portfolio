import React from "react";
import './ModalProject.css'
import { useModalContext } from "../../CustomState";
import { IoIosClose } from "react-icons/io";
import { useInfo } from "../../CustomState";
const ModalProject = (props)=>{
    
    const [modal,setModal]=useModalContext()
    const[modalInfo,setModalInfo] = useInfo()

    return(
        <div style={{opacity:props.opacity,pointerEvents:props.events}} className='modal-project modal-portfolio' id="modal" >
                <div className='modal-project-body'>
                    <div className="project-image" style={{backgroundImage:`url(${modalInfo.background})`}} >
                        <a onClick={()=>setModal(!modal)} className="close-modal"><IoIosClose/></a>
                    </div>
                    <div className="project-info">
                        <div className='project-name'>
                            <h2>{modalInfo.name}</h2>
                        </div>
                        <p>{modalInfo.description}</p>
                        <p>Desarrollado con <span>{modalInfo.developed}</span></p>
                        

                        <p>Sitio web: <a target="_blank" href={modalInfo.link}>{modalInfo.link}</a></p>
                        
                    </div>
                </div>
        </div>
    )
}

export default ModalProject