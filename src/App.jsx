import './App.css'
import Portfolio from './components/portfolio/Portfolio'
import Grid from './components/grid/Grid'
import { CustomRef } from './components/CustomRef'
import { CustomState } from './components/CustomState'
import { Routes,Route } from 'react-router-dom'
function App() {


  return (
    
      <main>
        <CustomState>
          <CustomRef>
            <Routes>
              <Route path='/' element={<Grid/>} />
              <Route path='/projects' element={<Portfolio/>} />
              <Route path="*" />
            </Routes>
          </CustomRef>
        </CustomState>
      </main>
      

  )
}

export default App
